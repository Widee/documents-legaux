Déclaration de confidentialité et de gestion des données personnelles sur le site et les créations de Widee.fr
==============================================================================================================

Sur le site Widee.fr et toutes les créations réalisées par Widee (ci-après dénommé le "Application"), nous nous engageons à protéger vos données personnelles. Cette déclaration de confidentialité s’applique à toutes les applications par Widee Robin FRÈRE (ayant le logo et le lien en pied de page ou dans les mentions légales). En accédant à l'application, vous acceptez l’intégralité de cette déclaration de confidentialité.

Dernière modification : 28 juillet 2020

LA RGPD
-------

Nous avons toujours travaillé dans le respect du visiteur et du client, et nous avons toujours fait en sorte d'apporter un soin extrême aux informations qui arrivent à nous. En lisant toutes les recommandations de la RGPD, nous avons constaté que nous les respections déjà en tous points. Nous souhaitons tout de même rapeller que tous les éléments d'une personne ayant accès à nos services aura tout loisir de faire valoir ses droits sur ses données personnelles.

Collecte de vos informations personnelles
-----------------------------------------

Nous recueillons des informations susceptibles d’être personnelles :
* Soit de l’enregistrement volontaire du visiteur qui remplit le formulaire d’inscription en vue d’utiliser les services de Widee Robin FRÈRE, en ayant accepté les conditions générales d’utilisation et de confidentialité.
* Soit de l’enregistrement des informations résultant d’un compte de client lié à la société propriétaire du domaine sur lequel est pointé cette application.
* Soit de l’enregistrement de l’adresse email dans le formulaire destiné à l’inscription à la newsletter.
* Soit de manière anonyme, des données qui proviennent de votre navigateur sur nos applications : le nom de votre fournisseur d’accès, la date et l’heure de connexion, l’adresse IP, clic sur un lien, consultation d’une page, recherche effectuée et tout ce qui est transmissible depuis le terminal du visiteur.


Ce que nous faisons de ces informations :
-----------------------------------------

Ces informations servent principalement à l’analyse des visiteurs dans le but d’améliorer nos services, et de vous garantir un meilleur service clientèle.

Les informations récoltées ne sont jamais transmises à des tiers.

Si vous vous êtes inscrit à notre service de communication, vous recevrez des informations de notre part sur l’évolution de nos programmes ou sur les actualités qui seront en rapport avec les choix que vous aurez établis dans votre espace de gestion personnel. Ces informations sont accessibles depuis votre espace personnel et seront modifiables selon vos préférences. Vous aurez également la possibilité de supprimer définitivement toutes vos informations depuis ce même espace. À l’exception des comptes qui seraient liés à une facturation, conformément au code des postes et des télécommunications électroniques (art. L34-1-III). En cas de suppression, l’opération sera définitive et ne pourra être restaurée.

Afin de garantir une meilleure communication (SMS, courrier, email …), un meilleur fonctionnement (hébergement, empaquetage, référencement, …) voire d’autres services, nous pouvons être amenés à utiliser des services tiers, nous ne transmettrons que les informations nécessaires au bon fonctionnement de ces services qui garantiront une totale discrétion sur la gestion des données personnelles. Ces entreprises sont tenues de respecter les informations transmises et ne pourront en aucun cas les utiliser dans un autre but.

Nous pouvons vous garantir que nous ne volons, vendons, louons, ou partageons pas vos informations avec d’autres parties.

Nous ne communiquerons vos données personnelles que si la loi l’exige, ou si nous pensons en toute bonne foi qu’il est nécessaire d’appliquer un décret de loi ou une procédure légale en vue de défendre ou protéger les droits et propriétés de chacun, et agir afin de protéger la sécurité personnelle de toute personne qui pourrait être concerné (visiteur, inscrit, administrateur, responsable, hébergeur, concepteur, toute personne ayant une relation directe ou indirecte avec l'application ou les services proposés).

Toutes les informations recueillies sur l'application peuvent être stockées et utilisées en France ou dans tout autre pays par ses filiales ou ses partenaires. En utilisant l'application, vous consentez à de tels transferts de vos informations personnelles en dehors de votre pays.

Conformément à la loi « informatique et libertés » du 6 janvier 1978 modifiée en 2004, vous bénéficiez d’un droit d’accès et de rectification aux informations qui vous concernent, que vous pouvez exercer en utilisant le formulaire de contact accessible par le lien “contactez-nous” en bas de page.

Sécurité de vos données personnelles
------------------------------------

Nous garantissons une protection totale de vos informations et de toutes les données de l'application et nous mettons tout en œuvre pour que la sécurité soit maximale afin qu’il n’y ait pas de fuite d’informations. Nous utilisons des technologies afin de garantir cette sécurité. Les mises à jour de sécurité sont effectuées en priorité, toutes les failles sont analysées et nous réalisons les modifications nécessaires afin de limiter une quelconque intrusion.

Chaque utilisateur est responsable de son mot de passe de connexion, et tout accès à ses informations par le biais de son mot de passe sera sous sa seule responsabilité.

Protection des informations personnelles des enfants
----------------------------------------------------

Ces données concernent uniquement les services qui sont directement liés à l’enfant, et en accord avec les parents. Certains de nos services peuvent être amenés à récolter des informations sur des enfants. Nous garantissons que celles-ci sont stockées sur une base de données qui est vidée chaque année et aucune sauvegarde n’est conservée. L’accès à ces informations sont totalement privées, non partagées.


Cookies
-------

Un cookie est un fichier posé sur votre terminal par l'application ou un de nos services, ou par un partenaire afin de vous apporter une navigation adaptée à vos besoins.

Il ne pourra pas être utilisé à des fins d’infection virale ou de piratage.

Nos cookies ne pourront être lus que par nos services et nos serveurs.

L’avantage principal de l’utilisation d’un cookie est de vous faire gagner du temps, notamment la mémorisation de vos identifiants afin d’être reconnu automatiquement à chaque visite, vous évitant ainsi de renseigner votre mot de passe à chaque connexion.

Vous avez la possibilité de refuser ou d’accepter les cookies sur votre terminal. La majorité des navigateurs les acceptent, en accédant aux paramètres de celui-ci vous aurez la possibilité de les refuser. Pour cela, nous vous invitons à vous diriger sur la notice d’utilisation de votre outil de navigation. Si vous choisissez de refuser les cookies, il se peut que vous n’ayez pas les accès attendus sur les applications que vous utiliserez, et vous risquez de perdre les fonctions interactives que proposent ces applications.

Application de cette déclaration de confidentialité
---------------------------------------------------

Tous vos commentaires sont les bienvenus, et si cette déclaration, vous semble incorrecte, ou si vous avez des questions sur ces conditions, vous pouvez nous contacter par le formulaire de contact disponible sur le site par le lien « contactez-nous ». Si vous constatez un manquement à une loi sur la dite déclaration, merci de nous le faire savoir en toute simplicité, nous nous engageons à respecter la loi et à apporter les modifications nécessaires dans un délai raisonnable.

La simple utilisation de nos applications, consent que vous acceptiez notre déclaration de confidentialité sans réserve.

Modifications de cette déclaration de confidentialité
-----------------------------------------------------

Cette déclaration étant susceptible d’être modifiée sans préavis, nous vous invitons à consulter cette page de temps en temps. Un affichage sur la page d’accueil pourra indiquer une modification de celle-ci dans le cas où une modification substantielle a été réalisée. Toute correction mineure (syntaxique, orthographique, lexicale) ne fera pas l’objet d’une communication. En tête de cette déclaration est affichée la date de dernière modification, vous permettant ainsi de savoir si cette déclaration a été modifiée.

