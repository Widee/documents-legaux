Conditions générales de services et d’utilisations :
====================================================

1. [Définitions](./CGS/01-definitions.md)
2. [Acceptation et modification des conditions générales de services et d’utilisation](./CGS/02-acceptation-des-cgs.md)
3. [Droit de propriété](./CGS/03-droit-et-propriete.md)
4. [Licence limitée](./CGS/04-licence-limitee.md)
5. [Publication sur le site](./CGS/05-publication-sur-le-site.md)
6. [Exclusion de garantie](./CGS/06-exclusion-de-garantie.md)
7. [Limitation de responsabilité](./CGS/07-limitation-de-responsabilite.md)
8. [Indemnisation](./CGS/08-indemnisation.md)
9. [Politique de confidentialité](./CGS/09-politique-de-confidentialite.md)
10. [Vérification des informations publiées](./CGS/10-verification-des-informations-publiees.md)
11. [Réclamation](./CGS/11-reclamation.md)
12. [Loi applicable et tribunal compétent](./CGS/12-loi-applicable.md)
13. [RGPD](./CGS/13-RGPD.md)
14. [Divisibilité](./CGS/14-divisibilite.md)
15. [Tarifs et facturation](./CGS/15-tarifs-et-facturation.md)
16. [Livraison d'application](./CGS/16-livraison.md)
17. [Mode de règlements](./CGS/17-mode-de-reglement.md)
18. [Retard de paiement](./CGS/18-retard-de-paiement.md)
19. [Résiliation de l’abonnement](./CGS/19-resiliation.md)
20. [Annulation de commande](./CGS/20-annulation-de-commande.md)
21. [Droit de rétractation](./CGS/21-droit-de-retractation.md)
22. [Garanties et délais de livraison](CGS/22-garanties-et-delai-de-livraison.md)

Date de dernière modification : 03 janvier 2022
