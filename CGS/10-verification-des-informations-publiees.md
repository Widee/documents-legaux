Vérification des informations publiées
======================================

>cette page fait l'objet d'un chapitre dont le sommaire est accessible sur la page [CONDITIONS GÉNÉRALES DE SERVICES](../CGS.md)

Le développeur n’a aucune obligation de vérification des informations présentes sur l'application ou publiées par un visiteur, un inscrit ou un client. Tout visiteur, inscrit ou client, ou toute personne physique ou morale, accepte sans réserve que le développeur contrôle à sa discrétion le contenu de celui-ci, les raisons peuvent être :
* Pour respecter une loi, un règlement, répondre à une demande des autorités.
* Suite à une demande d’une tierce personne signalant le nom respect d’une règle.
* Afin de vérifier le bon fonctionnement du site et apporter les modifications nécessaires.
* Pour protéger les visiteurs, les clients, les inscrits ou toute personne qui pourrait être concernée.
* Pour toute autre raison, et à sa propre initiative.

Le développeur se réserve le droit de supprimer, modifier, refuser toute publication qui ne respecterait pas les conditions générales d’utilisations ci présentes, ou serait une infraction au regard de la loi, sans préavis et en toute discrétion.

Si un visiteur, un inscrit, un client publie sur l'application des informations quel que soit leur type (données, réponse, texte, image, commentaire, idées, projet, ou tout autre genre d’information), elles seront considérées comme non confidentielles, et le développeur ne s’engage pas à les protéger.

[<< Chapitre précédent](./09-politique-de-confidentialite.md)

[Chapitre suivant >>](./11-reclamation.md)
