Acceptation et modification des conditions générales de services et d’utilisation
=================================================================================

>cette page fait l'objet d'un chapitre dont le sommaire est accessible sur la page [CONDITIONS GÉNÉRALES DE SERVICES](../CGS.md)

LE DÉVELOPPEUR détient et exploite ses services et applications. L’utilisation des services, l'utilisation de l'application implique l’acceptation sans réserve des conditions citées, appelées “conditions générales de services et d’utilisation”.

L’utilisateur des services de la société Widee Robin FRÈRE accepte et s’engage à respecter les règles et conditions édictées et toutes les conditions générales de services et d’utilisation.

La société Widee Robin FRÈRE se réserve le droit, sans préavis et sans avis de publication, d’apporter des modifications à tout ou partie des présentes conditions générales de services et d’utilisation, notamment dans le cadre des évolutions de ses services ou de la législation en cours et invite les utilisateurs à vérifier régulièrement les possibles modifications apportées aux présentes conditions générales.

[<< Chapitre précédent](./01-definitions.md)

[Chapitre suivant >>](./03-droit-et-propriete.md)
