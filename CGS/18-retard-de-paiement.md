Retard de paiement
==================

>cette page fait l'objet d'un chapitre dont le sommaire est accessible sur la page [CONDITIONS GÉNÉRALES DE SERVICES](../CGS.md)

Retard :
--------

Conformément à l’article L 441-6 du code du commerce, des pénalités de retard seront appliqués en cas de non-règlement de la facture à la date prévue par les présentes conditions générales de services et d’utilisation.

Pénalités et suspensions :
--------------------------

Les pénalités de retard seront égales au taux en vigueur comme il est prévu dans la législation, auxquels s’ajouteront l’indemnité pour les frais de recouvrement prévus également par la législation.

Les services liés à un abonnement, non réglés à la date d'échéance, engendrera une suspension du service, ce qui entraînera des frais afférents de suspension et de rétablissement de service.

Le montant minimal des frais de suspension s'élèvent à 20 euros par service.

Le montant minimal des frais de rétablissement s'élèvent à 20 euros par service.

Défaut de paiement :
--------------------

Pour tout échec de règlement (virement bancaire non autorisé, prélèvement par carte bancaire non autorisé, chèque non provisionné ...), un message d'alerte sera envoyé par email. Le règlement devra être réalisé dans les 5jours, au-delà de ce délai la facture sera considéré comme en défaut de paiement et les pénalités seront appliqués sans autre préavis.


[<< Chapitre précédent](./17-mode-de-reglement.md)

[Chapitre suivant >>](./19-resiliation.md)
