Modes de règlements
===================

>cette page fait l'objet d'un chapitre dont le sommaire est accessible sur la page [CONDITIONS GÉNÉRALES DE SERVICES](../CGS.md)

Les méthodes de paiements acceptées :
------------------------------------

Pour les montants inférieurs à 500 euros, sont acceptés les règlements par virements bancaires, par carte bancaire et en espèces (dans la devise spécifiée sur les factures).

Pour les montants supérieurs à 500 euros seuls les règlements par virements bancaires sont acceptés.

Les règlements par chèque ne sont plus acceptés. Des exceptions peuvent être accordées avec une facturation supplémentaire d'un minimum de 5 € (le montant exact sera défini lors de la facturation du service).

Les abonnements de services :
-----------------------------

Les abonnements de services pourront être réglés avec la mise en place d'un virement automatique auprès de la banque du client, au plus tard le 5 du mois.

Ils pourront également être réglés par carte bancaire, dans ce cas, le prélèvement sur la carte bancaire se fera de façon systématique lors du renouvellement de l'abonnement. (Pour résilier un abonnement, voir la section [Résiliation](./19-resiliation.md))

Utilisation du service de paiement par Carte Bancaire :
-------------------------------------------------------

En tant que prestataire de services, Widee Robin FRÈRE utilise plusieurs fournisseurs bancaires pour encaisser les sommes dûes aux clients :

* Stripe : permet de gérer les moyens de paiements numériques des clients en ligne. Il sert principalement pour les règlements des abonnements. Les empreintes de cartes bancaires sont mémorisées par Stripe, et en aucun cas sur les serveurs de Widee Robin FRÈRE. Ce service sera utilisé systématiquement et de manière automatique pour toutes les facturations, à partir du premier règlement numérique effectué en ligne sur un des sites internet de Widee Robin FRÈRE.
* Zettle (partenaire de Paypal) : Pour les règlements par terminal physique, ce service permet les encaissements ponctuels sur une carte bancaire, avec lequel le client pourra saisir son code personnel.

[<< Chapitre précédent](./16-livraison.md)

[Chapitre suivant >>](./18-retard-de-paiement.md)
