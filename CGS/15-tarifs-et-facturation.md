Tarifs et facturation
=====================

>cette page fait l'objet d'un chapitre dont le sommaire est accessible sur la page [CONDITIONS GÉNÉRALES DE SERVICES](../CGS.md)

Tous les tarifs proposés par le développeur sont proposés par devis avec le détail des prestations et des abonnements. Tous les abonnements sont renouvelables par tacite reconduction et résiliables à tout moment ([voir article résiliation](./19-resiliation.md)).

Tous les tarifs s'entendent hors taxes. Depuis le 1er janvier 2022 la T.V.A. est applicable sur tous les services et produits proposés par **LE DÉVELOPPEUR**. 

La signature du devis implique l’accord des présentes conditions générales sans réserve et l’engagement du client à s'acquitter des sommes à régler.

Les tarifs sont susceptibles d’être modifiés au fil du temps, ils seront signalés par email, soixante jours avant l’application des modifications aux clients qui ont souscrit un service renouvelable.

Pour les prestations de services, le tarif présent sur le devis sera ferme et définitif jusqu’à la livraison si la signature et la réception du devis se fait dans les soixante jours suivant l’édition de celui-ci.

Les tarifs sur le site internet widee.fr, et par extension tous les sous-domaines de widee.fr, sont modifiables sans préavis et applicables le jour même de leur changement.

Toute demande de fonctionnalité n’ayant pas été prévue dans le cahier des charges initial fera l’objet d’un devis et d’une facturation supplémentaire.

Pour tout service d’abonnement gratuit, le développeur se réserve le droit d’utiliser une zone de l'application pour l’affichage de campagnes publicitaires de son choix.

Les devis présentant des prestations facturées de façon horaire renseignent d’une estimation de temps sur une prestation demandée, si le temps effectif est supérieur au temps estimé, le client devra s'acquitter du surplus de facturation.

Le règlement des prestations devra s’effectuer en intégralité à l’issue de sa réalisation.

Pour les développements d'applications Web, un espace de test est mis en place afin que le client contrôle l'évolution du développement. À l'issue des travaux le projet ne sera livré, en version de production, qu'après réception du règlement.

Toute prestation d'un montant inférieur à 1000 euros devra être réglé lors de la signature de la proposition commerciale ou du contrat.

Aucun délai de paiement ne pourra être accordé, aux retards de paiements il sera appliqué une pénalité conformément au code de la consommation ([voir article “retard de paiement”](./18-retard-de-paiement.md)) avec éventuellement la suspension du service, ce qui entraînerait les frais afférents à cette suspension.

Les abonnements de services devront être réglés avant l’exécution de ceux-ci, c’est-à-dire qu’un abonnement mensuel doit être réglé en début de mois (au plus tard le 5 du mois), et un abonnement annuel au début de la mise en place et ensuite à chaque date anniversaire.

Dans le cas où le client ne fournit pas les éléments prévus sur le devis (textes, visuels, logos, etc. ) ou si les éléments ne sont pas au format attendus, alors un prestataire tiers se chargera, à la demande du développeur, de réaliser les éléments attendus. La facturation de ce prestataire restera à la charge du client.

[<< Chapitre précédent](./14-divisibilite.md)

[Chapitre suivant >>](./16-livraison.md)
