Licence limitée
===============

>cette page fait l'objet d'un chapitre dont le sommaire est accessible sur la page [CONDITIONS GÉNÉRALES DE SERVICES](../CGS.md)

La licence d’utilisation et d’exploitation de l'application étant non exclusive et non transférable, le visiteur a le droit d’imprimer tout visuel de l'application sur un lien destiné à cet effet ou par la fonction d’impression du terminal ou de télécharger les éléments accessibles directement depuis un lien sur l'application. Ceci uniquement pour un besoin personnel et non commercial, à condition de conserver en l’état et sans apporter la moindre modification à l’élément récupéré.

L'ensemble des cessions de droits d'auteurs est disponible sur la [LICENCE](../LICENCE.md) disponible à cette adresse : [https://www.widee.fr/licence](https://www.widee.fr/licence).

Le client s’engage à ne jamais distribuer, vendre, copier, dupliquer, louer, voler, transmettre, reproduire, publier, décompiler, désassembler le code source, la base de données et de manière non exhaustive tout élément de l'application. Il s’engage également, à ne pas reproduire la logique, la présentation, ni créer d’oeuvre similaire aux applications créées par Widee Robin FRÈRE, que ce soit pour une utilisation privée ou commerciale.

Le non-respect d’une de ces règles mettra fin à l’utilisation de la licence, automatiquement, et selon la clause de fin de contrat le développeur mettra fin, sans préavis, au contrat, tout en se réservant un droit de poursuites judiciaire.

[<< Chapitre précédent](./03-droit-et-propriete.md)

[Chapitre suivant >>](./05-publication-sur-le-site.md)
