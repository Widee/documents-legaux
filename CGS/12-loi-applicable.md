Loi applicable et tribunal compétent
====================================

>cette page fait l'objet d'un chapitre dont le sommaire est accessible sur la page [CONDITIONS GÉNÉRALES DE SERVICES](../CGS.md)

Ces conditions générales de services et d’utilisation sont régies par les lois Françaises. Son application et son interprétation ne limitent pas les droits accordés à l’utilisateur.

Certaines licences peuvent faire intervenir des tribunaux étrangers pour la résolution de litiges.

[<< Chapitre précédent](./11-reclamation.md)

[Chapitre suivant >>](./13-RGPD.md)
