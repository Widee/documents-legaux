Indemnisation
=============

>cette page fait l'objet d'un chapitre dont le sommaire est accessible sur la page [CONDITIONS GÉNÉRALES DE SERVICES](../CGS.md)

À sa demande, vous vous engagez à indemniser le développeur, ses sociétés mères ou affiliées, partenaires, filiales ou concédant de licence ainsi que leurs employés, contractants, agents et dirigeants, en cas de mise en cause de leur responsabilité, ainsi que de réclamations et frais (dont honoraires d’avocat) découlant de votre utilisation ou de vos erreurs dans l’utilisation de l'application. Dans la mesure du possible, vous acceptez de coopérer avec le développeur pour sa défense dans le cadre de toute affaire dans laquelle vous devriez offrir une indemnisation.

[<< Chapitre précédent](./07-limitation-de-responsabilite.md)

[Chapitre suivant >>](./09-politique-de-confidentialite.md)
