Définitions :
=============

>cette page fait l'objet d'un chapitre dont le sommaire est accessible sur la page [CONDITIONS GÉNÉRALES DE SERVICES](../CGS.md)

**Le VISITEUR** : La personne physique qui navigue et utilise les services d’un site internet, par le biais d’un outil de navigation internet.

**L’INSCRIT** : La personne physique ou morale utilisant les services internet ayant été reconnue à l’aide d’un identifiant et d’un mot de passe personnel, de son propre choix.

**L’IDENTIFIANT** : En général il s'agit de l'adresse email de la personne physique ou morale, permettant son identification ; il peut s'agir également d'un autre type tel qu'un numéro de compte client, un numéro de licence.

**Le CLIENT** : Personne physique ou morale ayant souscrit, au moins, à un service proposé par Widee Robin FRÈRE.

**L’HÉBERGEUR** : Entreprise qui se charge du stockage des informations et des bases de données des sites créés et administrés par Widee Robin FRÈRE qui se charge également de fournir les ressources pour l'exécution des scripts et des programmes de conceptions logicielles de Widee Robin FRÈRE.

**LE DÉVELOPPEUR** : Widee Robin FRÈRE, le concepteur du système qui apporte les services fournis sur les sites internet créés, gérés, administrés par Widee.

**Le NAVIGATEUR** : Logiciel qui permet la consultation des sites internet.

**L'APPLICATION** : Est un programme (ou un ensemble logiciel) directement utilisé pour réaliser une tâche, ou un ensemble de tâches élémentaires d'un même domaine ou formant un tout. Il est le résultat du travail du développeur.

**Le NOM DE DOMAINE** : adresse internet que l’on communique dans le navigateur du visiteur pour faciliter l’accès au site internet sur lequel il pointe.

[<< Retour au sommaire ](../CGS.md)

[Chapitre suivant >>](./02-acceptation-des-cgs.md)
