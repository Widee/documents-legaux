Publication sur le site
=======================

>cette page fait l'objet d'un chapitre dont le sommaire est accessible sur la page [CONDITIONS GÉNÉRALES DE SERVICES](../CGS.md)

En tant que visiteur, inscrit ou client nul n’est autorisé à utiliser l'application à des fins illégales et de façon non limitative nul n’a le droit de faire de publication :

* en vue d'harceler un tiers ou de compromettre sa vie privée
* d’informations erronées, obscènes, racistes, diffamatoires ou incitant à la haine.
* incitant au crime, au suicide, à la violence, à la discrimination.
* susceptible de nuire aux mineurs ou de porter atteinte à la dignité humaine.
* faisant l’apologie du nazisme, du terrorisme ou du négationnisme
* de données soumises à copyright, ou appartenant à une tierce personne dont vous n’auriez pas reçu l’autorisation de publication.

[<< Chapitre précédent](./04-licence-limitee.md)

[Chapitre suivant >>](./06-exclusion-de-garantie.md)
