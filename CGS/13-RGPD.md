RGPD
====

>cette page fait l'objet d'un chapitre dont le sommaire est accessible sur la page [CONDITIONS GÉNÉRALES DE SERVICES](../CGS.md)

Depuis le 25 mai 2018, le règlement européen sur la RGPD est applicable.

En tant que prestataire technique, tous les développements et tous les hébergements réalisés par Widee Robin FRÈRE sont soumis à la règlementation de la RGPD. À ce titre il rend le prestataire responsable techniquement des données qui transite par ses services. Tout est mis en oeuvre pour que la sécurité soit optimale et surveillée. Toutes les données ne sont accessibles par aucun autre tiers qui ne serait pas signalé au client.

Pour tout travail de sous-traitance le client final doit être informé des travaux réalisés par Widee Robin FRÈRE, cette information devra également figurer dans les mentions légales des produits réalisés, conformément au chapitre IV du règlement RGPD.

Les mentions légales devront faire apparaitre la phrase "Hébergement administré par l'EIRL Widee Robin FRÈRE / 114 clos vert bocage 01270 BEAUPONT" dans le cas d'un hébergement, et la phrase : "Application Web réalisée par l'EIRL Widee Robin FRÈRE / 114 clos vert bocage 01270 BEAUPONT" dans le cas d'un développement d'application. Pour toute autre type de réalisation, la phrase dépendra du service, et sera fournis à la livraison du produit. Les phrases de mentions légales devront être suivis d'un lien direct sur le site [www.widee.fr](https://www.widee.fr).

[<< Chapitre précédent](./11-reclamation.md)

[Chapitre suivant >>](./14-divisibilite.md)
