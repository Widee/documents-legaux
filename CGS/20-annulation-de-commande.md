Annulation de commande
======================

>cette page fait l'objet d'un chapitre dont le sommaire est accessible sur la page [CONDITIONS GÉNÉRALES DE SERVICES](../CGS.md)

L’annulation de la commande après signature et acceptation par le client, engendrera la perte des sommes versées.

L’annulation de la commande après 14 jours pour un particulier, induit que les sommes à payer, restent dues, cette clause de protection du consommateur de 14 jours de délai de rétractation ne s’applique pas pour les professionnels, ni les particuliers qui font la demande expresse de l’exécution du travail (voir droit de rétractation)

[<< Chapitre précédent](./19-resiliation.md)

[Chapitre suivant >>](./21-droit-de-retractation.md)
