Garanties et délai de livraison
===============================

>cette page fait l'objet d'un chapitre dont le sommaire est accessible sur la page [CONDITIONS GÉNÉRALES DE SERVICES](../CGS.md)

Garanties
---------

Les services rendus et réalisés par Widee Robin FRÈRE, selon un cahier des charges, sont garantis au client, correspondre au mieux à celui-ci . Après livraison, le client aura trente jours pour transmettre les dysfonctionnements afin qu’ils soient rectifiés dans un délai raisonnable.

Les services fournis sur des logiciels tiers, ou des services tiers seront soumis aux conditions du partenaire, et les modifications dépendront uniquement du dit partenaire selon ses propres conditions d’utilisation et de services.

Un formulaire de contact présent sur le site widee.fr permet également le signalement d’un dysfonctionnement fonctionnel ou visuel.

Délai de livraison
-------------------

Lors de la signature du devis, et après réception du premier acompte, un délai de livraison sera donné au client. Au vu des différents aléas que peuvent représenter tous les systèmes informatiques, et la nécessité de réaliser un projet totalement sécurisé, stable, avec tous les tests de fonctionnement, ce délai est totalement indicatif et ne pourra pas représenter une clause d'annulation de prestation, ou un quelconque dédommagement, sous quelque forme que ce soit.

Le délai de livraison pourra être revu à la hausse en cas de délai de la part du client pour fournir les différents éléments nécessaires au développement de l'application, ou si les éléments ne sont pas conformes à la demande du développeur.

En cas d'attente trop longue sur des fournitures d'éléments attendus par le développeur, celui-ci pourra alors passer au développement d'une application d'un autre client, et reprendre l'activité une fois l'autre terminée.
Ce qui, de fait, augmentera d'autant plus le délai de livraison.

[<< Chapitre précédent](./21-droit-de-retractation.md)

[Retour au sommaire >>](../CGS.md)
