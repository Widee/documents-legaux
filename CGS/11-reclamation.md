Réclamation
===========

>cette page fait l'objet d'un chapitre dont le sommaire est accessible sur la page [CONDITIONS GÉNÉRALES DE SERVICES](../CGS.md)

Dans le cas d’une contrefaçon la victime d’une contrefaçon, ou d’un délit constaté sur l'application', sera invitée à le signaler par mail à l’adresse : [contact@widee.fr](contact@widee.fr).

Dans le cas du non-respect d’une règle des présentes conditions générales ou d’une loi, vous pouvez également nous en faire part par email à l’adresse : [contact@widee.fr](contact@widee.fr).

Nous mettrons tout en œuvre pour contrôler et analyser la réclamation et porter les actions nécessaires en fonction de l’infraction dans un délai raisonnable.

[<< Chapitre précédent](./10-verification-des-informations-publiees.md)

[Chapitre suivant >>](./12-loi-applicable.md)
