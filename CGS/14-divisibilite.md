Divisibilité
============

>cette page fait l'objet d'un chapitre dont le sommaire est accessible sur la page [CONDITIONS GÉNÉRALES DE SERVICES](../CGS.md)

Si une partie des présentes conditions générales venait à être invalidée par une loi, ou déclarée inapplicable, elle devra être comparée à la législation en vigueur, et le reste des conditions générales sera toujours applicable et ne sera ni invalidé ni annulé.

[<< Chapitre précédent](./13-RGPD.md)

[Chapitre suivant >>](./15-tarifs-et-facturation.md)