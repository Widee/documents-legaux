Résiliation de l’abonnement
===========================

>cette page fait l'objet d'un chapitre dont le sommaire est accessible sur la page [CONDITIONS GÉNÉRALES DE SERVICES](../CGS.md)

La résiliation aux différents services proposés par L’HÉBERGEUR se font par lettre recommandée avec accusé réception au plus tard le 15 du mois qui précède la facturation.

Le service concerné sera arrêté le jour de réception du courrier.

Les sommes engagées ne seront pas remboursées et le client s’engage à régler les sommes dues dès réception de la facture de clôture, dans le cas contraire les pénalités de retard s’appliqueront.

[<< Chapitre précédent](./18-retard-de-paiement.md)

[Chapitre suivant >>](./20-annulation-de-commande.md)
