Livraison
=========

>cette page fait l'objet d'un chapitre dont le sommaire est accessible sur la page [CONDITIONS GÉNÉRALES DE SERVICES](../CGS.md)

On parle ici de livraison d'application (produit immatériel), et non pas de produit matériel. Widee Robin FRÈRE ne réalise aucune livraison de matériel.

Hébergement à la charge de Widee Robin FRÈRE :
----------------------------------------------

Sans demande spécifique, lors de la signature du contrat, la livraison de l'application est implicitement réalisée sur les serveurs administrés par Widee Robin FRÈRE. Le coût de la mise en place de l'application, du transfert du nom de domaine est compris dans la réalisation de l'application. Les services d'hébergements feront l'objet d'une facturation dédiée.

Le choix des options des services d'hébergements se fera avant la mise en production de l'application.

Le client peut à tout moment demander le code source de l'application et se charger lui-même de l'hébergement. Dans ce cas les règles liées à "l'hébergement à la charge du client" s'appliqueront.

La livraison physique du code source annule toute responsabilité du développeur ainsi que tous les hébergements liés.

Hébergement à la charge du client :
-----------------------------------

Le client peut, à sa demande, héberger lui-même l'application sur les serveurs de son choix.

Il devra au préalable fournir un document déchargeant Widee Robin FRÈRE de toute responsabilité en cas de fuite de données, dysfonctionnement de l'application, de manière générale de tout engagement réciproque.

Le client devra également fournir les coordonnées complets de la personne morale responsable de l'hébergement, et du délégué à la protection des données. Ces deux personnes morales (pouvant être la même) devront justifier de leurs compétences dans les domaines qui leurs seront attribués.

Après la publication sur un serveur de test (administré par Widee Robin FRÈRE), le client pourra tester toutes les fonctionnalités de l'application, si toutes les fonctionnalités sont validées par le client, il devra régler le solde de la commande, l'application lui sera alors envoyé par courrier recommandé avec accusé de réception sur un support numérique. Toutes les informations de mise en place feront partie de l'application et devront être appliquées par le responsable de l'hébergement.

Toute assistance à la mise en place de l'application sera facturée selon la grille tarifaire en vigueur.

Il restera à la charge du client de modifier les mentions légales de l'application pour faire apparaitre les informations d'hébergements. Nous rappelons qu'en aucun cas les informations du développeur ne doivent être supprimées.

[<< Chapitre précédent](./15-tarifs-et-facturation.md)

[Chapitre suivant >>](./17-mode-de-reglement.md)
