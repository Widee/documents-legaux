Limitation de responsabilité
============================

>cette page fait l'objet d'un chapitre dont le sommaire est accessible sur la page [CONDITIONS GÉNÉRALES DE SERVICES](../CGS.md)

Widee Robin FRÈRE, comme aucun de ses partenaires, qu'il soit développeur, hébergeur, sous-traitant ou affilié, n'est responsable de dommages réels ou supposés, par manque de résultat, de bénéfice, de clientèle ou perte de données et, ceci, de façon non limitative, même si le développeur, ou l’hébergeur ont été informés de la possibilité d’un dommage dans ce sens.

L’utilisation et la capacité d’utiliser l'application ou son contenu comme son accession n'incombe pas à la société Widee Robin FRÈRE, ni à ses partenaires ou ses ayants-droit, cette limitation s’applique en matière délictuelle comme contractuelle.

[<< Chapitre précédent](./06-exclusion-de-garantie.md)

[Chapitre suivant >>](./08-indemnisation.md)
