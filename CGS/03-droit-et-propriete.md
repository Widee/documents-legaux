Droit de propriété
==================

>cette page fait l'objet d'un chapitre dont le sommaire est accessible sur la page [CONDITIONS GÉNÉRALES DE SERVICES](../CGS.md)

L’ensemble du contenu et des éléments disponibles sur les applications, de façon non limitative, les images, les textes, les vidéos, les documents sonores, les logos, les bases de données, les codes sources, …, toutes ces informations accessibles ou non sont protégées par copyright, brevets, droits de propriété intellectuelles, secrets industriels et autres droits, et ne doivent pas être copiés, dupliqués, volés, utilisés sans l’accord écrit, daté et signé du développeur, sous peine de dépôt de plainte auprès du tribunal compétant relatif à l’acte effectué.

Pour information : chaque élément et chaque texte de l'application a une justification de création datée et scellée afin de fournir la preuve de la création originale.

Les licences des éléments de sources externes présents sur l'application s’appliquent selon leurs conditions à chacune.

Pour publier sur l'application un élément, une information, un document, un logo soumit à un droit quel qu’il soit, il est impératif de se prémunir d’une autorisation écrite de la part de l’auteur ou du propriétaire de la licence.

[<< Chapitre précédent](./02-acceptation-des-cgs.md)

[Chapitre suivant >>](./04-licence-limitee.md)
