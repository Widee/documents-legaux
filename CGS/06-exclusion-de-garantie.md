Exclusion de garantie
=====================

>cette page fait l'objet d'un chapitre dont le sommaire est accessible sur la page [CONDITIONS GÉNÉRALES DE SERVICES](../CGS.md)

Le développeur apporte une attention particulière au bon fonctionnement du site internet, la gestion des services ou des serveurs, il se peut que l'application contienne des inexactitudes techniques ou des erreurs typographiques.

Le développeur se réserve le droit de modifier les applications, programmes, présentations, accès, toute information ou contenu de manière discrète et sans préavis. Des maintenances de tout type peuvent survenir à tout moment de la journée ou de la nuit, engendrant un ralentissement ou une inaccessibilité de l'application, le développeur s’emploie à, toujours, tout mettre en oeuvre pour rétablir les services dans les meilleurs délais. En cas de rupture totale des services l’utilisateur est prié de contacter le service client de Widee Robin FRÈRE.
Ni le développeur, ni l’hébergeur, ni aucun partenaire proposant les services de Widee Robin FRÈRE n’offrent de garantie quant à leur contenu.
Les utilisateurs assumeront-totalement tous les risques d’utilisation.
Aucune responsabilité n’incombe au développeur, ni même à ses fournisseurs, partenaires ou hébergeurs quant à l’utilisation, publication ou éléments présents et contenus sur l'application.

AUCUNE GARANTIE NE S'APPLIQUE SUR LES CONTENUS, qu'ils soient formels ou non .

Aucun avis ou information, fournis par le développeur, l’hébergeur, leurs ayants droit ou leurs employés ne peuvent offrir de garantie, ni sur la disponibilité de l'application en tout lieu et à toute heure, la fiabilité, la précision ou l’exactitude du contenu, de n'importe quel élément de l'application, comme de l’absence de virus ou d’éléments nuisibles dans les contenus, les informations et les logiciels ou tout élément accessible à partir de l'application.

[<< Chapitre précédent](./05-publication-sur-le-site.md)

[Chapitre suivant >>](./07-limitation-de-responsabilite.md)
