Droit de rétractation
=====================

>cette page fait l'objet d'un chapitre dont le sommaire est accessible sur la page [CONDITIONS GÉNÉRALES DE SERVICES](../CGS.md)

Applicable uniquement si le client est un particulier, le client dispose d’un délai de 14jours pour faire valoir son droit de rétractation. L’avis de rétractation doit être communiqué par courrier avec accusé de réception à l’adresse mentionnée dans les mentions légales. Il n’est pas applicable selon l’article de loi du code de la consommation L121-21-5 si le client a fait la demande de l’exécution du service, et si tout ou partie du service a été réalisé avant la fin de la période de rétractation.

[<< Chapitre précédent](./20-annulation-de-commande.md)

[Chapitre suivant >>](./22-garanties-et-delai-de-livraison.md)
