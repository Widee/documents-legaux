Politique de confidentialité
============================

>cette page fait l'objet d'un chapitre dont le sommaire est accessible sur la page [CONDITIONS GÉNÉRALES DE SERVICES](../CGS.md)

Le développeur a pour vocation de protéger les données personnelles de chacun. Pour avoir accès à la déclaration de confidentialité [cliquez ici](../CONFIDENTIALITE.md). L’accord aux présentes conditions générales implique l’accord à la politique de confidentialité de Widee Robin FRÈRE.

[<< Chapitre précédent](./08-indemnisation.md)

[Chapitre suivant >>](./10-verification-des-informations-publiees.md)