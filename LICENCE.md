Copyright © 2015-2022 Widee Robin FRÈRE
=========================================

Cession de droits d’auteurs
--------------------------

Conformément au « Code de la propriété intellectuelle - Article L131-3 », toutes les créations artistiques commercialisées, facturées et réglées sont soumises aux conditions de transfert de droits intellectuels ci-après citées :

Les créations livrées sont identifiées en plusieurs parties :

* Les images (photos, créations graphiques, logo, icônes, …)
* Le code HTML (permettant l’interprétation visuelle du site par les navigateurs)
* Le code javascript (programme exécuté et interprété par le navigateur)
* Le code CSS (Permet la présentation et l’organisation visuelle du site internet)
* Les frameworks (ensemble cohérent de composants logiciels structurels, qui sert à créer les fondations ainsi que les grandes lignes de tout ou d’une partie d'un logiciel.)
* La conception (l’ensemble des lignes de code permettant la liaison entre tous les éléments et permettra le rendu final avec la logique demandée dans le cahier des charges)

Les frameworks sont transmis et utilisables avec leurs droits d’auteurs respectifs, ils seront listés sur le Bon de livraison du produit final.

Droit de reproduction
---------------------

Pour des raisons de sécurités et de risque de fuites d’informations, aucun droit de copie, d’impression, de duplication ou de reproduction n’est autorisé. Il est d’ailleurs recommandé de mettre sous clés dans un endroit sécurisé le code source fourni.

Droit d’utilisation et d’exploitation
------------------------------------

Le code source sera utilisé uniquement sur un serveur dont le client sera administrateur, et ne pourra pas faire l’objet d’un partage ou d’une revente à quelque partie que ce soit.

Droit de modification
---------------------

Le code répondant à la logique de son concepteur, chaque élément ayant des inter-actions avec tout ou partie du programme, pour des raisons de sécurités évidente et dans le but de maintenir l’application stable, toute modification devra faire l’objet d’un courrier explicatif du changement apporté avec les raisons, et sera validé, ou non, par son concepteur.

Droit de traduction
-------------------

le logiciel pourra faire l’objet d’une traduction et utilisable en multi langue, si l’option « multilingue » a été choisi lors de la signature du contrat. Les conditions de traductions seront livrées avec le produit final, ainsi que les langues ayant la possibilité d’être traduites.

Droit d’adaptation
------------------

Tout ou partie du code source ne pourra pas faire l’objet d’une adaptation à d’autres fins que celles listé dans le cahier des charges fourni par le client, et ne pourra pas, par conséquent, être modifié.

Durée d’exploitation
--------------------

L’exploitation de l’œuvre n’est limitée à aucune durée d’exploitation.

Lieu d’exploitation
------------------

Pour une application Web, son exploitation sera limitée sur le nom de domaine prévu lors de la signature du devis ou du contrat.

Pour une application Mobile, son exploitation sera limitée aux plateformes de vente d'applications dédiées listées dans le contrat ou le devis.

Abus d'utilisation
------------------

Le non-respect d’une de ces règles mettra fin à l’utilisation de la licence, automatiquement, et selon la clause de fin de contrat le développeur mettra fin, sans préavis, au contrat, tout en se réservant un droit de poursuites judiciaire.
