# Documents légaux de l'entreprise Widee Robin FRERE

## La licence applicable aux créations de Widee Robin FRERE

La licence de la totalité des créations de Widee Robin FRERE est disponible dans le fichier [LICENCE.md](./LICENCE.md), cette licence est applicable à toutes les créations et à tous les projets fournis par l'entreprise Widee Robin FRERE, sauf mention contraire dans le fichier README du projet fourni.

## Les conditions générales de services

L'ensemble des conditions générales de services sont accessibles sont accessibles à cette adresse : [CGS.md](./CGS.md).

## Déclaration de confidentialité et gestion des données personnelles
 
Sur le site Widee.fr et toutes les créations réalisées par Widee (ci-après dénommé le "Site internet"), nous nous engageons à protéger vos données personnelles. Cette [déclaration de confidentialité](./CONFIDENTIALITE.md) s’applique à tous les sites réalisés par widee.fr (ayant le logo et le lien en pied de page). En accédant au Site internet, vous acceptez l’intégralité de cette déclaration de confidentialité.